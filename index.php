<?php
require_once 'db.php';
$courseId = 3;

$sql = '
select mdl_user.id,mdl_user.username,mdl_user.firstname,mdl_user.lastname from mdl_user 
INNER JOIN mdl_user_enrolments on mdl_user.id = mdl_user_enrolments.userid 
INNER JOIN mdl_enrol on mdl_user_enrolments.enrolid = mdl_enrol.id 
INNER JOIN mdl_course on mdl_enrol.courseid = mdl_course.id 
WHERE mdl_user.id = mdl_user_enrolments.userid AND mdl_course.id = ?
';
$stmt = $pdo->prepare($sql);
$stmt->execute([$courseId]);
$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sql = 'select id,fullname from mdl_course where enddate > ? and id > 1';
$currentTime = time();
$stmt = $pdo->prepare($sql);
$stmt->execute([$currentTime]);
$courses = $stmt->fetchAll(PDO::FETCH_ASSOC);
$sql= '';

if(isset($_GET['error'])){
    echo $_GET['error'];
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css">
    <title>Document</title>
</head>
<body>
    <div class="main-wrapper">
        <div class="wrap">
            <div class="container">
                <div class="form">
                    <form action="api.php" method="post">
                        <div class="inp">
                            <label for="user">Choose proctor</label>
                            <select name="user" id="user">
                                <?php foreach($users as $user): ?>
                                    <option value="<?php echo $user['id'] ?>"><?php echo $user['firstname'] ?> <?php echo $user['lastname'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="inp">
                            <label for="date">Выберите дату</label>
                            <input type="date" name="date">
                        </div>
                        <div class="inp">
                            <label for="title">Назовите экзамен</label>
                            <input type="text" name="title">
                        </div>
                        <div class="inp">
                            <label for="time">Выберите время</label>
                            <input type="time" name="time" id="time">
                        </div>
                        <div class="inp">
                            <label for="course">Choose course</label>
                            <select name="course" id="course">
                                <?php foreach($courses as $course): ?>
                                    <option value="<?php echo $course['id'] ?>"><?php echo $course['fullname'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="inp">
                            <label for="group">Choose group</label>
                            <div id="group-div">
                                <select name="group" id="group">

                                </select>
                            </div>
                        </div>
                        <div class="inp">
                            <label for="quiz">Choose quiz</label>
                            <div id="quiz-div">
                                <select name="quiz" id="quiz">

                                </select>
                            </div>
                        </div>
                        <button class="btn" name="addProctor">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
<script src="js/app.js"></script>
</html>

