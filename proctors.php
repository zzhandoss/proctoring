<?php
require_once 'db.php';
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $qid = $_GET['quizid'];
}else{
    exit('400 Bad Request');
}
$sql = 'select * from mdl_proctor where id = ?';
$stmt = $pdo->prepare($sql);
$stmt->execute([$id]);
$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
$groupid = $res['0']['groupid'];
$sql = 'select mdl_user.id,mdl_user.username,mdl_user.firstname,mdl_user.lastname,mdl_groups.name,mdl_sessions.sid from mdl_user
        inner join mdl_groups_members on mdl_user.id = mdl_groups_members.userid
        inner join mdl_groups on mdl_groups_members.groupid = mdl_groups.id
        left join mdl_sessions on  mdl_user.id = mdl_sessions.userid
        where mdl_groups.id = ? order by mdl_user.id';
$stmt = $pdo->prepare($sql);
$stmt->execute([$groupid]);
$res = $stmt->fetchAll(PDO::FETCH_ASSOC);
$students = $res;
var_dump($students);
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Proctoring Page</title>
    <link rel="stylesheet" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/bootstrap/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="proctor.css">
    <title>Просмотр видео и экрана участника</title>
    <script type="text/javascript" src="lib/flashphoner.js"></script>
    <script type="text/javascript" src="lib/jquery/jquery-1.12.0.js"></script>
    <script type="text/javascript" src="lib/js/utils.js"></script>
    <script type="text/javascript" src="proctor.js"></script>
    <!-- Bootstrap JS -->
    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
<br>
<hr>
<button id="startExam" onclick="startExam()" class="btn btn-success">Start exam</button>
<br><br>
<button id="endExam" onclick="endExam()" class="btn btn-danger" disabled>End exam</button>
<br>
<hr>
    <div class="wrapper">
        <?php
        $index = 0;
        foreach($students as $user):
            $index++;
        ?>
            <div class="col-sm-6">
                <h3><?php echo $user['name']?></h3>
                <div class="text-center text-muted"><?php echo $user['firstname'].' '.$user['lastname'] ?></div>
                <div class="fp-remoteVideo">
                    <div id="player<?php echo $index ?>" class="display"></div>
                </div>

                <div id="form<?php echo $index ?>" class="input-group flex-direction-column" style="margin: 10px auto 0 auto;flex-direction: column;display: flex">
                    <input class="form-control" type="text" id="streamName<?php echo $index ?>" value="<?php echo $user['id'].'in'.$qid ?>">
                    <input type="hidden" id="userQuiz<?php echo $index ?>" value="<?php echo $user['id'].'_'.$qid ?>">
                    <input type="hidden" id="userSession<?php echo $index ?>" value="<?php echo  $user['sid'] ? $user['sid'] : '' ?>">
                    <div class="input-group-btn">
                        <button id="playBtn<?php echo $index ?>" type="button" class="btn btn-default">Play</button>
                        <br>
                        <button id="snapshotBtn<?php echo $index ?>" type="button" class="btn btn-default">Snapshot</button>
                    </div>
                </div>
                <div class="text-center" style="margin-top: 20px">
                    <div id="status<?php echo $index ?>"></div>
                </div>
                <div id="myModal<?php echo $index ?>" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Snapshot камеры</h4>
                            </div>
                            <div class="modal-body">
                                <img id="dynImg<?php echo $index ?>" class="fp-snapshot"/>
                            </div>
                            <div class="modal-footer">
                                <a class="btn btn-default" id ="link<?php echo $index ?>" download>Скачать Snapshot камеры</a>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
        <input id="userCount" value="<?php echo $index ?>" type="hidden" />
    </div>
</body>
</html>