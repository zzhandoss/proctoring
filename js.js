document.addEventListener("DOMContentLoaded", function(event) {

    console.log(document.cookie);

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    console.log(getCookie('MoodleSession'))
    let cookie = getCookie('MoodleSession');
    return new Promise((resolve,reject) => {
        const request = new XMLHttpRequest();
        const url = "http:localhost/moodle/api.php?cookie="+cookie;
        request.open('GET', url);
        request.setRequestHeader('Content-Type', 'application/json');
        request.setRequestHeader('Access-Control-Allow-Origin', '*');
        request.addEventListener("readystatechange", () => {
            if (request.readyState === 4 && request.status === 200) {
                console.log( request.responseText );
                resolve(request.responseText);
            }
        });
        request.send();
    }).then((data)=>{
        let response = JSON.parse(data);
        console.log(response)
        response.map((item)=>{
            console.log(item);
            let doc = document.createElement('div');
            document.body.appendChild(doc);
            doc.innerHTML='<div id="proctor-box" style="padding: 15px;border:1px solid silver; width:500px;display: grid;grid-template-columns: 1fr 1fr; font-size: 16px; justify-items: center">\n' +
                '        <span>Название:</span>\n' +
                '        <span>'+item.title+'</span>\n' +
                '        <span>Время:</span>\n' +
                '        <span>'+item.time+'</span>\n' +
                '        <span>Дата:</span>\n' +
                '        <span>'+item.date+'</span>\n' +
                '        <span>Ссылка</span>\n' +
                '        <span>http://localhost/proctors?id='+item.id+'</span>\n' +
                '    </div>'
        });
    }).catch((data)=>{
        console.log(data);
    })
});


