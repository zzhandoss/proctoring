document.addEventListener("DOMContentLoaded", function(event) {
    let courseSelect = document.getElementById('course');
    let a = '<option value="1">1</option><option value="2">orem ipsum.</option>'
    function createGroupOption(group){
        document.getElementById("group").append(
            '<option value="'+group.id+'">'+group.name+'</option>'
        )
    }
    console.log(courseSelect);
    courseSelect.addEventListener('input', function () {
        console.log(this.value);
        document.getElementById("group").innerHTML = '';
        let id = this.value;
        return new Promise((resolve,reject) => {
            const request = new XMLHttpRequest();
            const url = "api.php?courseid="+id;
            request.open('GET', url);
            request.setRequestHeader('Content-Type', 'application/json');
            request.addEventListener("readystatechange", () => {
                if (request.readyState === 4 && request.status === 200) {
                    console.log( request.responseText );
                    resolve(request.responseText);
                }
            });
            request.send();
        }).then((data)=>{
            let response = JSON.parse(data);
            console.log(response);
            response.groups.map((item) => {
                let option = document.createElement("option");
                option.text = item.name;
                option.value = item.id;
                document.getElementById("group").appendChild(option);
            });
            response.quiz.map((item)=>{
                console.log(item);
                let option = document.createElement("option");
                option.text = item.id+'. '+item.name;
                option.value = item.id;
                document.getElementById("quiz").appendChild(option);

            });

        }).catch((data)=>{
            console.log(data);
        })
    })




});