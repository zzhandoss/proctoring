<?php
if (isset($_SERVER['HTTP_ORIGIN'])) {
    // should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
    // whitelist of safe domains
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}
    require_once 'db.php';

    if(isset($_GET['courseid'])){
        $id = $_GET['courseid'];
        $sql = 'SELECT id,courseid,name from mdl_groups where courseid = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$id]);
        if($stmt){
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $answer['groups'] = $result;
            $sql = 'SELECT mdl_quiz.id,name,timecreated from mdl_quiz
            inner join mdl_course_modules on mdl_quiz.id = mdl_course_modules.instance
             where mdl_course_modules.course = ? and mdl_course_modules.module = 17';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$id]);
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $answer['quiz'] = $res;
            exit(json_encode($answer));
        }
    }
    if(isset($_GET['cookie'])){
        $cookie = $_GET['cookie'];
        $sql = 'SELECT userid from mdl_sessions where sid = ?';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$cookie]);
        $id = null;
        if($stmt){
            $result = $stmt->fetch(PDO::FETCH_LAZY);
            $id = $result['userid'];
        }else{
            exit('{error:No such cookie or user}');
        }
        $currentDate = date('Y-m-d');
        $sql = 'SELECT * from mdl_proctor where userid = ? and date >= ?';
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$id,$currentDate]);
        if($stmt){
            $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
            exit(json_encode($result));
        }else{
            exit('{error:No data about proctor}');
        }
    }

    if(isset($_POST['addProctor'])){
        $uid = $_POST['user'];
        $date = $_POST['date'];
        $title = $_POST['title'];
        $time = $_POST['time'];
        $quiz = $_POST['quiz'];
        $courseid = $_POST['course'];
        $groupid = $_POST['group'];
        $sql = "insert into mdl_proctor(userid,date,title,time,courseid,groupid,quizId) values(?,?,?,?,?,?,?)";
        $stmt = $pdo->prepare($sql);
        $params = [$uid, $date, $title, $time, $courseid, $groupid,$quiz];
        if($stmt->execute($params)){
            header('Location: http://localhost:81/moodle');
        }else{
            header('Location: http://localhost/moodle/index.php?error=Error');
        }
    }
    if(isset($_GET['cook'])){
            $cookie = $_GET['cook'];
            $cmid = $_GET['cmid'];
            $sql = 'SELECT userid from mdl_sessions where sid = ?';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$cookie]);
            $id = null;
            if($stmt){
                $result = $stmt->fetch(PDO::FETCH_LAZY);
                $userId = $result['userid'];
            }else{
                exit('{error:No such cookie or user}');
            }
            $currentDate = date('Y-m-d');
            $sql = 'SELECT mdl_quiz.id from mdl_quiz
             inner join mdl_course_modules on mdl_quiz.id = mdl_course_modules.instance
             where mdl_course_modules.id = ?';
            $stmt = $pdo->prepare($sql);
            $stmt->execute([$cmid]);
            if($stmt){
                $result = $stmt->fetch(PDO::FETCH_LAZY);
                $quizId = $result['id'];
                $answer['quizId'] = $quizId;
                $answer['userId'] = $userId;
                exit(json_encode($answer));
            }else{
                exit('{error:No data about proctor}');
            }
        }